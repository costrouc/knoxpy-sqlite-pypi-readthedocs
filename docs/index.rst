.. sshlog documentation master file, created by
   sphinx-quickstart on Wed Mar 28 16:03:39 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sshlog's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   support


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
